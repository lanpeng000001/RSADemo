package com.gitee.li_yu_jiang.logger;

/**
 * 日志打印器
 *
 * @author 大定府羡民
 */
public interface IPrinter {

    void print(String msg);

}