# RSADemo
RSA算法工具类及其demo   

1. 公钥加密，私钥解密。
2. 私钥签名，公钥验证。

### 封装的方法
```text
genKeyPairByJKSFile
genKeyPairByRandom
encodeToString
encrypt
decrypt
sign
verify
obtainPublicKeyFormEncoded
obtainPrivateKeyFormEncoded
obtainPublicKeyFromFile
obtainPrivateKeyFromFile
obtainPublicKeyFromModulus
obtainPrivateKeyFromModulus
printPublicKeyInfo
printPrivateKeyInfo
```
